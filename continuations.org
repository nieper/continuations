#+TITLE: Continuations
#+SUBTITLE: Proper tail recursion, CPS transformation, and Call/CC
#+AUTHOR: Marc Nieper-Wißkirchen
#+LANGUAGE: de-DE
#+OPTIONS: H:2 toc:t num:t
#+LATEX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [presentation]
#+LATEX_HEADER: \usepackage[]{babel}
#+BEAMER_THEME: Madrid

* Tail calls

* Proper tail recursion

* Continuations

* CPS transformation

** The CPS transformation is a Yoneda embedding

* Call-with-current-continuation

* Continuation marks
